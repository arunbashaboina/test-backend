export interface IBusinessArea {
  id?: string;
  name?: string;
  description?: string;
  agents?: string;
  type?: string;
}

export class BusinessArea implements IBusinessArea {
  constructor(public id?: string, public name?: string, public description?: string, public agents?: string, public type?: string) {}
}
