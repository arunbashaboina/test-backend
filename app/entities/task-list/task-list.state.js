(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('task-list', {
            parent: 'entity',
            url: '/task-list?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.taskList.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/task-list/task-lists.html',
                    controller: 'TaskListController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('taskList');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('task-list-detail', {
            parent: 'task-list',
            url: '/task-list/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.taskList.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/task-list/task-list-detail.html',
                    controller: 'TaskListDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('taskList');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TaskList', function($stateParams, TaskList) {
                    return TaskList.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'task-list',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('task-list-detail.edit', {
            parent: 'task-list-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/task-list/task-list-dialog.html',
                    controller: 'TaskListDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TaskList', function(TaskList) {
                            return TaskList.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('task-list.new', {
            parent: 'task-list',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/task-list/task-list-dialog.html',
                    controller: 'TaskListDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                title: null,
                                notes: null,
                                startDate: null,
                                dueDate: null,
                                completed: null,
                                starred: null,
                                important: null,
                                deleted: null,
                                workSpace: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('task-list', null, { reload: 'task-list' });
                }, function() {
                    $state.go('task-list');
                });
            }]
        })
        .state('task-list.edit', {
            parent: 'task-list',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/task-list/task-list-dialog.html',
                    controller: 'TaskListDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TaskList', function(TaskList) {
                            return TaskList.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('task-list', null, { reload: 'task-list' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('task-list.delete', {
            parent: 'task-list',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/task-list/task-list-delete-dialog.html',
                    controller: 'TaskListDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TaskList', function(TaskList) {
                            return TaskList.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('task-list', null, { reload: 'task-list' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
