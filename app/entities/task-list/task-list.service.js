(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('TaskList', TaskList);

    TaskList.$inject = ['$resource', 'DateUtils'];

    function TaskList ($resource, DateUtils) {
        var resourceUrl =  'api/task-lists/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.startDate = DateUtils.convertDateTimeFromServer(data.startDate);
                        data.dueDate = DateUtils.convertDateTimeFromServer(data.dueDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
