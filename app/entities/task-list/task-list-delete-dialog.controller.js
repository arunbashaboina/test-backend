(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('TaskListDeleteController',TaskListDeleteController);

    TaskListDeleteController.$inject = ['$uibModalInstance', 'entity', 'TaskList'];

    function TaskListDeleteController($uibModalInstance, entity, TaskList) {
        var vm = this;

        vm.taskList = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TaskList.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
