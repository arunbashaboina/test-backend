(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('TaskListDetailController', TaskListDetailController);

    TaskListDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TaskList'];

    function TaskListDetailController($scope, $rootScope, $stateParams, previousState, entity, TaskList) {
        var vm = this;

        vm.taskList = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:taskListUpdate', function(event, result) {
            vm.taskList = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
