(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('TaskListDialogController', TaskListDialogController);

    TaskListDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TaskList'];

    function TaskListDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TaskList) {
        var vm = this;

        vm.taskList = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.taskList.id !== null) {
                TaskList.update(vm.taskList, onSaveSuccess, onSaveError);
            } else {
                TaskList.save(vm.taskList, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:taskListUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.startDate = false;
        vm.datePickerOpenStatus.dueDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
