(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('CalenderDetailController', CalenderDetailController);

    CalenderDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Calender'];

    function CalenderDetailController($scope, $rootScope, $stateParams, previousState, entity, Calender) {
        var vm = this;

        vm.calender = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:calenderUpdate', function(event, result) {
            vm.calender = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
