(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('CalenderDialogController', CalenderDialogController);

    CalenderDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Calender'];

    function CalenderDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Calender) {
        var vm = this;

        vm.calender = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.calender.id !== null) {
                Calender.update(vm.calender, onSaveSuccess, onSaveError);
            } else {
                Calender.save(vm.calender, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:calenderUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
