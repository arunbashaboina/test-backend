(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('Calender', Calender);

    Calender.$inject = ['$resource'];

    function Calender ($resource) {
        var resourceUrl =  'api/calenders/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
