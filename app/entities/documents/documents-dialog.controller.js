(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocumentsDialogController', DocumentsDialogController);

    DocumentsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Documents'];

    function DocumentsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Documents) {
        var vm = this;

        vm.documents = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.documents.id !== null) {
                Documents.update(vm.documents, onSaveSuccess, onSaveError);
            } else {
                Documents.save(vm.documents, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:documentsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
