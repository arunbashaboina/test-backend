(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('documents', {
            parent: 'entity',
            url: '/documents',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.documents.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/documents/documents.html',
                    controller: 'DocumentsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('documents');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('documents-detail', {
            parent: 'documents',
            url: '/documents/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.documents.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/documents/documents-detail.html',
                    controller: 'DocumentsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('documents');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Documents', function($stateParams, Documents) {
                    return Documents.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'documents',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('documents-detail.edit', {
            parent: 'documents-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents/documents-dialog.html',
                    controller: 'DocumentsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Documents', function(Documents) {
                            return Documents.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('documents.new', {
            parent: 'documents',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents/documents-dialog.html',
                    controller: 'DocumentsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                title: null,
                                description: null,
                                type: null,
                                siteId: null,
                                parentId: null,
                                mimeType: null,
                                size: null,
                                archiveId: null,
                                archiveDocId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('documents', null, { reload: 'documents' });
                }, function() {
                    $state.go('documents');
                });
            }]
        })
        .state('documents.edit', {
            parent: 'documents',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents/documents-dialog.html',
                    controller: 'DocumentsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Documents', function(Documents) {
                            return Documents.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('documents', null, { reload: 'documents' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('documents.delete', {
            parent: 'documents',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents/documents-delete-dialog.html',
                    controller: 'DocumentsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Documents', function(Documents) {
                            return Documents.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('documents', null, { reload: 'documents' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
