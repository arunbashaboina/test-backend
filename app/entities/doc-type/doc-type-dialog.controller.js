(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocTypeDialogController', DocTypeDialogController);

    DocTypeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DocType'];

    function DocTypeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DocType) {
        var vm = this;

        vm.docType = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.docType.id !== null) {
                DocType.update(vm.docType, onSaveSuccess, onSaveError);
            } else {
                DocType.save(vm.docType, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:docTypeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
