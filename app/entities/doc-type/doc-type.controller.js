(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocTypeController', DocTypeController);

    DocTypeController.$inject = ['$scope', '$state', 'DocType'];

    function DocTypeController ($scope, $state, DocType) {
        var vm = this;

        vm.docTypes = [];

        loadAll();

        function loadAll() {
            DocType.query(function(result) {
                vm.docTypes = result;
                vm.searchQuery = null;
            });
        }
    }
})();
