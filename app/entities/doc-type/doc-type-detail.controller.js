(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocTypeDetailController', DocTypeDetailController);

    DocTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DocType'];

    function DocTypeDetailController($scope, $rootScope, $stateParams, previousState, entity, DocType) {
        var vm = this;

        vm.docType = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:docTypeUpdate', function(event, result) {
            vm.docType = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
