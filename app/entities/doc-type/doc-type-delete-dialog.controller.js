(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocTypeDeleteController',DocTypeDeleteController);

    DocTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'DocType'];

    function DocTypeDeleteController($uibModalInstance, entity, DocType) {
        var vm = this;

        vm.docType = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DocType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
