(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('WorkspaceDialogController', WorkspaceDialogController);

    WorkspaceDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Workspace'];

    function WorkspaceDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Workspace) {
        var vm = this;

        vm.workspace = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.workspace.id !== null) {
                Workspace.update(vm.workspace, onSaveSuccess, onSaveError);
            } else {
                Workspace.save(vm.workspace, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:workspaceUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
