(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('WorkspaceDetailController', WorkspaceDetailController);

    WorkspaceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Workspace'];

    function WorkspaceDetailController($scope, $rootScope, $stateParams, previousState, entity, Workspace) {
        var vm = this;

        vm.workspace = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:workspaceUpdate', function(event, result) {
            vm.workspace = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
