(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('Binder', Binder);

    Binder.$inject = ['$resource'];

    function Binder ($resource) {
        var resourceUrl =  'api/binders/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
