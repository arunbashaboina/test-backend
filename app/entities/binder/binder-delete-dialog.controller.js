(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('BinderDeleteController',BinderDeleteController);

    BinderDeleteController.$inject = ['$uibModalInstance', 'entity', 'Binder'];

    function BinderDeleteController($uibModalInstance, entity, Binder) {
        var vm = this;

        vm.binder = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Binder.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
