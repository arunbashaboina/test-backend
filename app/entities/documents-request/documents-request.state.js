(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('documents-request', {
            parent: 'entity',
            url: '/documents-request?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.documentsRequest.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/documents-request/documents-requests.html',
                    controller: 'DocumentsRequestController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('documentsRequest');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('documents-request-detail', {
            parent: 'documents-request',
            url: '/documents-request/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.documentsRequest.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/documents-request/documents-request-detail.html',
                    controller: 'DocumentsRequestDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('documentsRequest');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DocumentsRequest', function($stateParams, DocumentsRequest) {
                    return DocumentsRequest.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'documents-request',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('documents-request-detail.edit', {
            parent: 'documents-request-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents-request/documents-request-dialog.html',
                    controller: 'DocumentsRequestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DocumentsRequest', function(DocumentsRequest) {
                            return DocumentsRequest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('documents-request.new', {
            parent: 'documents-request',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents-request/documents-request-dialog.html',
                    controller: 'DocumentsRequestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                documentId: null,
                                requester: null,
                                status: null,
                                approver: null,
                                requestType: null,
                                siteId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('documents-request', null, { reload: 'documents-request' });
                }, function() {
                    $state.go('documents-request');
                });
            }]
        })
        .state('documents-request.edit', {
            parent: 'documents-request',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents-request/documents-request-dialog.html',
                    controller: 'DocumentsRequestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DocumentsRequest', function(DocumentsRequest) {
                            return DocumentsRequest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('documents-request', null, { reload: 'documents-request' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('documents-request.delete', {
            parent: 'documents-request',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents-request/documents-request-delete-dialog.html',
                    controller: 'DocumentsRequestDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DocumentsRequest', function(DocumentsRequest) {
                            return DocumentsRequest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('documents-request', null, { reload: 'documents-request' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
