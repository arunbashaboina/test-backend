(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('ApprovalMatrixDetailController', ApprovalMatrixDetailController);

    ApprovalMatrixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ApprovalMatrix'];

    function ApprovalMatrixDetailController($scope, $rootScope, $stateParams, previousState, entity, ApprovalMatrix) {
        var vm = this;

        vm.approvalMatrix = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:approvalMatrixUpdate', function(event, result) {
            vm.approvalMatrix = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
