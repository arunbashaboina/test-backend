(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('ApprovalMatrix', ApprovalMatrix);

    ApprovalMatrix.$inject = ['$resource'];

    function ApprovalMatrix ($resource) {
        var resourceUrl =  'api/approval-matrices/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
