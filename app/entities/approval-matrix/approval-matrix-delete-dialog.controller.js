(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('ApprovalMatrixDeleteController',ApprovalMatrixDeleteController);

    ApprovalMatrixDeleteController.$inject = ['$uibModalInstance', 'entity', 'ApprovalMatrix'];

    function ApprovalMatrixDeleteController($uibModalInstance, entity, ApprovalMatrix) {
        var vm = this;

        vm.approvalMatrix = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ApprovalMatrix.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
