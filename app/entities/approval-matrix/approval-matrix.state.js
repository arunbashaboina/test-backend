(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('approval-matrix', {
            parent: 'entity',
            url: '/approval-matrix?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.approvalMatrix.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/approval-matrix/approval-matrices.html',
                    controller: 'ApprovalMatrixController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('approvalMatrix');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('approval-matrix-detail', {
            parent: 'approval-matrix',
            url: '/approval-matrix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.approvalMatrix.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/approval-matrix/approval-matrix-detail.html',
                    controller: 'ApprovalMatrixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('approvalMatrix');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ApprovalMatrix', function($stateParams, ApprovalMatrix) {
                    return ApprovalMatrix.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'approval-matrix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('approval-matrix-detail.edit', {
            parent: 'approval-matrix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/approval-matrix/approval-matrix-dialog.html',
                    controller: 'ApprovalMatrixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ApprovalMatrix', function(ApprovalMatrix) {
                            return ApprovalMatrix.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('approval-matrix.new', {
            parent: 'approval-matrix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/approval-matrix/approval-matrix-dialog.html',
                    controller: 'ApprovalMatrixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                requesttype: null,
                                noOfApprovalLevels: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('approval-matrix', null, { reload: 'approval-matrix' });
                }, function() {
                    $state.go('approval-matrix');
                });
            }]
        })
        .state('approval-matrix.edit', {
            parent: 'approval-matrix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/approval-matrix/approval-matrix-dialog.html',
                    controller: 'ApprovalMatrixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ApprovalMatrix', function(ApprovalMatrix) {
                            return ApprovalMatrix.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('approval-matrix', null, { reload: 'approval-matrix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('approval-matrix.delete', {
            parent: 'approval-matrix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/approval-matrix/approval-matrix-delete-dialog.html',
                    controller: 'ApprovalMatrixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ApprovalMatrix', function(ApprovalMatrix) {
                            return ApprovalMatrix.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('approval-matrix', null, { reload: 'approval-matrix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
