(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('ApprovalMatrixDialogController', ApprovalMatrixDialogController);

    ApprovalMatrixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ApprovalMatrix'];

    function ApprovalMatrixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ApprovalMatrix) {
        var vm = this;

        vm.approvalMatrix = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.approvalMatrix.id !== null) {
                ApprovalMatrix.update(vm.approvalMatrix, onSaveSuccess, onSaveError);
            } else {
                ApprovalMatrix.save(vm.approvalMatrix, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:approvalMatrixUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
