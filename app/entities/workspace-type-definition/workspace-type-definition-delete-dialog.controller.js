(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('Workspace_type_definitionDeleteController',Workspace_type_definitionDeleteController);

    Workspace_type_definitionDeleteController.$inject = ['$uibModalInstance', 'entity', 'Workspace_type_definition'];

    function Workspace_type_definitionDeleteController($uibModalInstance, entity, Workspace_type_definition) {
        var vm = this;

        vm.workspace_type_definition = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Workspace_type_definition.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
