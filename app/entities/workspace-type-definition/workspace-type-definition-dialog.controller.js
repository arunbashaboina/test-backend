(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('Workspace_type_definitionDialogController', Workspace_type_definitionDialogController);

    Workspace_type_definitionDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Workspace_type_definition'];

    function Workspace_type_definitionDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Workspace_type_definition) {
        var vm = this;

        vm.workspace_type_definition = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.workspace_type_definition.id !== null) {
                Workspace_type_definition.update(vm.workspace_type_definition, onSaveSuccess, onSaveError);
            } else {
                Workspace_type_definition.save(vm.workspace_type_definition, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:workspace_type_definitionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
