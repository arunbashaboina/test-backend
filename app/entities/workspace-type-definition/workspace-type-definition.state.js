(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('workspace-type-definition', {
            parent: 'entity',
            url: '/workspace-type-definition?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.workspace_type_definition.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/workspace-type-definition/workspace-type-definitions.html',
                    controller: 'Workspace_type_definitionController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('workspace_type_definition');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('workspace-type-definition-detail', {
            parent: 'workspace-type-definition',
            url: '/workspace-type-definition/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.workspace_type_definition.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/workspace-type-definition/workspace-type-definition-detail.html',
                    controller: 'Workspace_type_definitionDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('workspace_type_definition');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Workspace_type_definition', function($stateParams, Workspace_type_definition) {
                    return Workspace_type_definition.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'workspace-type-definition',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('workspace-type-definition-detail.edit', {
            parent: 'workspace-type-definition-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/workspace-type-definition/workspace-type-definition-dialog.html',
                    controller: 'Workspace_type_definitionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Workspace_type_definition', function(Workspace_type_definition) {
                            return Workspace_type_definition.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('workspace-type-definition.new', {
            parent: 'workspace-type-definition',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/workspace-type-definition/workspace-type-definition-dialog.html',
                    controller: 'Workspace_type_definitionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                workspaceTypeName: null,
                                businessobjectId: null,
                                binderId: null,
                                workflowExists: null,
                                workflowId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('workspace-type-definition', null, { reload: 'workspace-type-definition' });
                }, function() {
                    $state.go('workspace-type-definition');
                });
            }]
        })
        .state('workspace-type-definition.edit', {
            parent: 'workspace-type-definition',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/workspace-type-definition/workspace-type-definition-dialog.html',
                    controller: 'Workspace_type_definitionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Workspace_type_definition', function(Workspace_type_definition) {
                            return Workspace_type_definition.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('workspace-type-definition', null, { reload: 'workspace-type-definition' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('workspace-type-definition.delete', {
            parent: 'workspace-type-definition',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/workspace-type-definition/workspace-type-definition-delete-dialog.html',
                    controller: 'Workspace_type_definitionDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Workspace_type_definition', function(Workspace_type_definition) {
                            return Workspace_type_definition.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('workspace-type-definition', null, { reload: 'workspace-type-definition' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
