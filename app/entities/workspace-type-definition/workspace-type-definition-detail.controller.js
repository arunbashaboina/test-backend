(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('Workspace_type_definitionDetailController', Workspace_type_definitionDetailController);

    Workspace_type_definitionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Workspace_type_definition'];

    function Workspace_type_definitionDetailController($scope, $rootScope, $stateParams, previousState, entity, Workspace_type_definition) {
        var vm = this;

        vm.workspace_type_definition = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:workspace_type_definitionUpdate', function(event, result) {
            vm.workspace_type_definition = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
