(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('ContactsDialogController', ContactsDialogController);

    ContactsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Contacts'];

    function ContactsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Contacts) {
        var vm = this;

        vm.contacts = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.contacts.id !== null) {
                Contacts.update(vm.contacts, onSaveSuccess, onSaveError);
            } else {
                Contacts.save(vm.contacts, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:contactsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.birthDay = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
