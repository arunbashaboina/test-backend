(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('contacts', {
            parent: 'entity',
            url: '/contacts?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.contacts.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contacts/contacts.html',
                    controller: 'ContactsController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contacts');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('contacts-detail', {
            parent: 'contacts',
            url: '/contacts/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.contacts.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contacts/contacts-detail.html',
                    controller: 'ContactsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contacts');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Contacts', function($stateParams, Contacts) {
                    return Contacts.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'contacts',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('contacts-detail.edit', {
            parent: 'contacts-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contacts/contacts-dialog.html',
                    controller: 'ContactsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Contacts', function(Contacts) {
                            return Contacts.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('contacts.new', {
            parent: 'contacts',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contacts/contacts-dialog.html',
                    controller: 'ContactsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                firstName: null,
                                lastName: null,
                                nickName: null,
                                phoneNumber: null,
                                email: null,
                                company: null,
                                jobTitle: null,
                                birthDay: null,
                                address: null,
                                notes: null,
                                type: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('contacts', null, { reload: 'contacts' });
                }, function() {
                    $state.go('contacts');
                });
            }]
        })
        .state('contacts.edit', {
            parent: 'contacts',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contacts/contacts-dialog.html',
                    controller: 'ContactsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Contacts', function(Contacts) {
                            return Contacts.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('contacts', null, { reload: 'contacts' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('contacts.delete', {
            parent: 'contacts',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contacts/contacts-delete-dialog.html',
                    controller: 'ContactsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Contacts', function(Contacts) {
                            return Contacts.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('contacts', null, { reload: 'contacts' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
