(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('ContactsDetailController', ContactsDetailController);

    ContactsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Contacts'];

    function ContactsDetailController($scope, $rootScope, $stateParams, previousState, entity, Contacts) {
        var vm = this;

        vm.contacts = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:contactsUpdate', function(event, result) {
            vm.contacts = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
