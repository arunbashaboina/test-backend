(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SubstituteDetailController', SubstituteDetailController);

    SubstituteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Substitute'];

    function SubstituteDetailController($scope, $rootScope, $stateParams, previousState, entity, Substitute) {
        var vm = this;

        vm.substitute = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:substituteUpdate', function(event, result) {
            vm.substitute = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
