(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('substitute', {
            parent: 'entity',
            url: '/substitute',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.substitute.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/substitute/substitutes.html',
                    controller: 'SubstituteController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('substitute');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('substitute-detail', {
            parent: 'substitute',
            url: '/substitute/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.substitute.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/substitute/substitute-detail.html',
                    controller: 'SubstituteDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('substitute');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Substitute', function($stateParams, Substitute) {
                    return Substitute.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'substitute',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('substitute-detail.edit', {
            parent: 'substitute-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/substitute/substitute-dialog.html',
                    controller: 'SubstituteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Substitute', function(Substitute) {
                            return Substitute.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('substitute.new', {
            parent: 'substitute',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/substitute/substitute-dialog.html',
                    controller: 'SubstituteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                userId: null,
                                substituteuser: null,
                                fromDate: null,
                                toDate: null,
                                status: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('substitute', null, { reload: 'substitute' });
                }, function() {
                    $state.go('substitute');
                });
            }]
        })
        .state('substitute.edit', {
            parent: 'substitute',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/substitute/substitute-dialog.html',
                    controller: 'SubstituteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Substitute', function(Substitute) {
                            return Substitute.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('substitute', null, { reload: 'substitute' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('substitute.delete', {
            parent: 'substitute',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/substitute/substitute-delete-dialog.html',
                    controller: 'SubstituteDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Substitute', function(Substitute) {
                            return Substitute.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('substitute', null, { reload: 'substitute' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
