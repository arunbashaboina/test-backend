(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('Substitute', Substitute);

    Substitute.$inject = ['$resource', 'DateUtils'];

    function Substitute ($resource, DateUtils) {
        var resourceUrl =  'api/substitutes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.fromDate = DateUtils.convertDateTimeFromServer(data.fromDate);
                        data.toDate = DateUtils.convertDateTimeFromServer(data.toDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
