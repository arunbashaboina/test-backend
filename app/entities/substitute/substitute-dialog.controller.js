(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SubstituteDialogController', SubstituteDialogController);

    SubstituteDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Substitute'];

    function SubstituteDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Substitute) {
        var vm = this;

        vm.substitute = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.substitute.id !== null) {
                Substitute.update(vm.substitute, onSaveSuccess, onSaveError);
            } else {
                Substitute.save(vm.substitute, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:substituteUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.fromDate = false;
        vm.datePickerOpenStatus.toDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
