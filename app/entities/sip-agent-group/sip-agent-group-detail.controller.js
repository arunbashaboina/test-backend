(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SipAgentGroupDetailController', SipAgentGroupDetailController);

    SipAgentGroupDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SipAgentGroup'];

    function SipAgentGroupDetailController($scope, $rootScope, $stateParams, previousState, entity, SipAgentGroup) {
        var vm = this;

        vm.sipAgentGroup = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:sipAgentGroupUpdate', function(event, result) {
            vm.sipAgentGroup = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
