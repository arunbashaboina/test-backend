(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('SipAgentGroup', SipAgentGroup);

    SipAgentGroup.$inject = ['$resource'];

    function SipAgentGroup ($resource) {
        var resourceUrl =  'api/sip-agent-groups/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
