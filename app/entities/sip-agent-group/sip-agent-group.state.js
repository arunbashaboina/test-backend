(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('sip-agent-group', {
            parent: 'entity',
            url: '/sip-agent-group?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.sipAgentGroup.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sip-agent-group/sip-agent-groups.html',
                    controller: 'SipAgentGroupController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('sipAgentGroup');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('sip-agent-group-detail', {
            parent: 'sip-agent-group',
            url: '/sip-agent-group/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.sipAgentGroup.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sip-agent-group/sip-agent-group-detail.html',
                    controller: 'SipAgentGroupDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('sipAgentGroup');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SipAgentGroup', function($stateParams, SipAgentGroup) {
                    return SipAgentGroup.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'sip-agent-group',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('sip-agent-group-detail.edit', {
            parent: 'sip-agent-group-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sip-agent-group/sip-agent-group-dialog.html',
                    controller: 'SipAgentGroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SipAgentGroup', function(SipAgentGroup) {
                            return SipAgentGroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sip-agent-group.new', {
            parent: 'sip-agent-group',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sip-agent-group/sip-agent-group-dialog.html',
                    controller: 'SipAgentGroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                userId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('sip-agent-group', null, { reload: 'sip-agent-group' });
                }, function() {
                    $state.go('sip-agent-group');
                });
            }]
        })
        .state('sip-agent-group.edit', {
            parent: 'sip-agent-group',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sip-agent-group/sip-agent-group-dialog.html',
                    controller: 'SipAgentGroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SipAgentGroup', function(SipAgentGroup) {
                            return SipAgentGroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sip-agent-group', null, { reload: 'sip-agent-group' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sip-agent-group.delete', {
            parent: 'sip-agent-group',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sip-agent-group/sip-agent-group-delete-dialog.html',
                    controller: 'SipAgentGroupDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SipAgentGroup', function(SipAgentGroup) {
                            return SipAgentGroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sip-agent-group', null, { reload: 'sip-agent-group' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
