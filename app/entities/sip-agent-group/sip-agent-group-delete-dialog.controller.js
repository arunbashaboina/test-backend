(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SipAgentGroupDeleteController',SipAgentGroupDeleteController);

    SipAgentGroupDeleteController.$inject = ['$uibModalInstance', 'entity', 'SipAgentGroup'];

    function SipAgentGroupDeleteController($uibModalInstance, entity, SipAgentGroup) {
        var vm = this;

        vm.sipAgentGroup = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SipAgentGroup.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
