(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SipAgentGroupDialogController', SipAgentGroupDialogController);

    SipAgentGroupDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SipAgentGroup'];

    function SipAgentGroupDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SipAgentGroup) {
        var vm = this;

        vm.sipAgentGroup = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.sipAgentGroup.id !== null) {
                SipAgentGroup.update(vm.sipAgentGroup, onSaveSuccess, onSaveError);
            } else {
                SipAgentGroup.save(vm.sipAgentGroup, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:sipAgentGroupUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
