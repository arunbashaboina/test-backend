(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('RequestActivityDialogController', RequestActivityDialogController);

    RequestActivityDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'RequestActivity'];

    function RequestActivityDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, RequestActivity) {
        var vm = this;

        vm.requestActivity = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.requestActivity.id !== null) {
                RequestActivity.update(vm.requestActivity, onSaveSuccess, onSaveError);
            } else {
                RequestActivity.save(vm.requestActivity, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:requestActivityUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
