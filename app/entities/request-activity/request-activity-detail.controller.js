(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('RequestActivityDetailController', RequestActivityDetailController);

    RequestActivityDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'RequestActivity'];

    function RequestActivityDetailController($scope, $rootScope, $stateParams, previousState, entity, RequestActivity) {
        var vm = this;

        vm.requestActivity = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:requestActivityUpdate', function(event, result) {
            vm.requestActivity = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
