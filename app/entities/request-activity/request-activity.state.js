(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('request-activity', {
            parent: 'entity',
            url: '/request-activity',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.requestActivity.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/request-activity/request-activities.html',
                    controller: 'RequestActivityController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('requestActivity');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('request-activity-detail', {
            parent: 'request-activity',
            url: '/request-activity/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.requestActivity.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/request-activity/request-activity-detail.html',
                    controller: 'RequestActivityDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('requestActivity');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'RequestActivity', function($stateParams, RequestActivity) {
                    return RequestActivity.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'request-activity',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('request-activity-detail.edit', {
            parent: 'request-activity-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/request-activity/request-activity-dialog.html',
                    controller: 'RequestActivityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['RequestActivity', function(RequestActivity) {
                            return RequestActivity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('request-activity.new', {
            parent: 'request-activity',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/request-activity/request-activity-dialog.html',
                    controller: 'RequestActivityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                actvity: null,
                                description: null,
                                buttonText: null,
                                url: null,
                                type: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('request-activity', null, { reload: 'request-activity' });
                }, function() {
                    $state.go('request-activity');
                });
            }]
        })
        .state('request-activity.edit', {
            parent: 'request-activity',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/request-activity/request-activity-dialog.html',
                    controller: 'RequestActivityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['RequestActivity', function(RequestActivity) {
                            return RequestActivity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('request-activity', null, { reload: 'request-activity' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('request-activity.delete', {
            parent: 'request-activity',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/request-activity/request-activity-delete-dialog.html',
                    controller: 'RequestActivityDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['RequestActivity', function(RequestActivity) {
                            return RequestActivity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('request-activity', null, { reload: 'request-activity' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
