import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BusinessArea } from 'app/shared/model/business-area.model';
import { BusinessAreaService } from './business-area.service';
import { BusinessAreaComponent } from './business-area.component';
import { BusinessAreaDetailComponent } from './business-area-detail.component';
import { BusinessAreaUpdateComponent } from './business-area-update.component';
import { BusinessAreaDeletePopupComponent } from './business-area-delete-dialog.component';
import { IBusinessArea } from 'app/shared/model/business-area.model';

@Injectable({ providedIn: 'root' })
export class BusinessAreaResolve implements Resolve<IBusinessArea> {
  constructor(private service: BusinessAreaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BusinessArea> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<BusinessArea>) => response.ok),
        map((businessArea: HttpResponse<BusinessArea>) => businessArea.body)
      );
    }
    return of(new BusinessArea());
  }
}

export const businessAreaRoute: Routes = [
  {
    path: 'business-area',
    component: BusinessAreaComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'smartportalApp.businessArea.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'business-area/:id/view',
    component: BusinessAreaDetailComponent,
    resolve: {
      businessArea: BusinessAreaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'smartportalApp.businessArea.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'business-area/new',
    component: BusinessAreaUpdateComponent,
    resolve: {
      businessArea: BusinessAreaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'smartportalApp.businessArea.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'business-area/:id/edit',
    component: BusinessAreaUpdateComponent,
    resolve: {
      businessArea: BusinessAreaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'smartportalApp.businessArea.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const businessAreaPopupRoute: Routes = [
  {
    path: 'business-area/:id/delete',
    component: BusinessAreaDeletePopupComponent,
    resolve: {
      businessArea: BusinessAreaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'smartportalApp.businessArea.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
