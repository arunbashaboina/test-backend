import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBusinessArea } from 'app/shared/model/business-area.model';

@Component({
  selector: '-business-area-detail',
  templateUrl: './business-area-detail.component.html'
})
export class BusinessAreaDetailComponent implements OnInit {
  businessArea: IBusinessArea;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ businessArea }) => {
      this.businessArea = businessArea;
    });
  }

  previousState() {
    window.history.back();
  }
}
