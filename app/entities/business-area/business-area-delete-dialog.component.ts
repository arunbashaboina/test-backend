import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBusinessArea } from 'app/shared/model/business-area.model';
import { BusinessAreaService } from './business-area.service';

@Component({
  selector: '-business-area-delete-dialog',
  templateUrl: './business-area-delete-dialog.component.html'
})
export class BusinessAreaDeleteDialogComponent {
  businessArea: IBusinessArea;

  constructor(
    private businessAreaService: BusinessAreaService,
    public activeModal: NgbActiveModal,
    private eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: string) {
    this.businessAreaService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'businessAreaListModification',
        content: 'Deleted an businessArea'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: '-business-area-delete-popup',
  template: ''
})
export class BusinessAreaDeletePopupComponent implements OnInit, OnDestroy {
  private ngbModalRef: NgbModalRef;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ businessArea }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BusinessAreaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.businessArea = businessArea;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
