import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IBusinessArea } from 'app/shared/model/business-area.model';
import { BusinessAreaService } from './business-area.service';

@Component({
  selector: '-business-area-update',
  templateUrl: './business-area-update.component.html'
})
export class BusinessAreaUpdateComponent implements OnInit {
  businessArea: IBusinessArea;
  isSaving: boolean;

  constructor(private businessAreaService: BusinessAreaService, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ businessArea }) => {
      this.businessArea = businessArea;
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    if (this.businessArea.id !== undefined) {
      this.subscribeToSaveResponse(this.businessAreaService.update(this.businessArea));
    } else {
      this.subscribeToSaveResponse(this.businessAreaService.create(this.businessArea));
    }
  }

  private subscribeToSaveResponse(result: Observable<HttpResponse<IBusinessArea>>) {
    result.subscribe((res: HttpResponse<IBusinessArea>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  private onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  private onSaveError() {
    this.isSaving = false;
  }
}
