import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBusinessArea } from 'app/shared/model/business-area.model';

type EntityResponseType = HttpResponse<IBusinessArea>;
type EntityArrayResponseType = HttpResponse<IBusinessArea[]>;

@Injectable({ providedIn: 'root' })
export class BusinessAreaService {
  public resourceUrl = SERVER_API_URL + 'api/business-areas';

  constructor(private http: HttpClient) {}

  create(businessArea: IBusinessArea): Observable<EntityResponseType> {
    return this.http.post<IBusinessArea>(this.resourceUrl, businessArea, { observe: 'response' });
  }

  update(businessArea: IBusinessArea): Observable<EntityResponseType> {
    return this.http.put<IBusinessArea>(this.resourceUrl, businessArea, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IBusinessArea>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBusinessArea[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
