import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SmartportalSharedModule } from 'app/shared';
import {
  BusinessAreaComponent,
  BusinessAreaDetailComponent,
  BusinessAreaUpdateComponent,
  BusinessAreaDeletePopupComponent,
  BusinessAreaDeleteDialogComponent,
  businessAreaRoute,
  businessAreaPopupRoute
} from './';

const ENTITY_STATES = [...businessAreaRoute, ...businessAreaPopupRoute];

@NgModule({
  imports: [SmartportalSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BusinessAreaComponent,
    BusinessAreaDetailComponent,
    BusinessAreaUpdateComponent,
    BusinessAreaDeleteDialogComponent,
    BusinessAreaDeletePopupComponent
  ],
  entryComponents: [
    BusinessAreaComponent,
    BusinessAreaUpdateComponent,
    BusinessAreaDeleteDialogComponent,
    BusinessAreaDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SmartportalBusinessAreaModule {}
