(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocumentsRequestsUserDeleteController',DocumentsRequestsUserDeleteController);

    DocumentsRequestsUserDeleteController.$inject = ['$uibModalInstance', 'entity', 'DocumentsRequestsUser'];

    function DocumentsRequestsUserDeleteController($uibModalInstance, entity, DocumentsRequestsUser) {
        var vm = this;

        vm.documentsRequestsUser = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DocumentsRequestsUser.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
