(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocumentsRequestsUserDialogController', DocumentsRequestsUserDialogController);

    DocumentsRequestsUserDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DocumentsRequestsUser'];

    function DocumentsRequestsUserDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DocumentsRequestsUser) {
        var vm = this;

        vm.documentsRequestsUser = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.documentsRequestsUser.id !== null) {
                DocumentsRequestsUser.update(vm.documentsRequestsUser, onSaveSuccess, onSaveError);
            } else {
                DocumentsRequestsUser.save(vm.documentsRequestsUser, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:documentsRequestsUserUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
