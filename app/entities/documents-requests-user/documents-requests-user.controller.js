(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocumentsRequestsUserController', DocumentsRequestsUserController);

    DocumentsRequestsUserController.$inject = ['$scope', '$state', 'DocumentsRequestsUser'];

    function DocumentsRequestsUserController ($scope, $state, DocumentsRequestsUser) {
        var vm = this;

        vm.documentsRequestsUsers = [];

        loadAll();

        function loadAll() {
            DocumentsRequestsUser.query(function(result) {
                vm.documentsRequestsUsers = result;
                vm.searchQuery = null;
            });
        }
    }
})();
