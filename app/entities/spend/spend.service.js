(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('Spend', Spend);

    Spend.$inject = ['$resource', 'DateUtils'];

    function Spend ($resource, DateUtils) {
        var resourceUrl =  'api/spends/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.date = DateUtils.convertDateTimeFromServer(data.date);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
