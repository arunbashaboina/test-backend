(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SpendDialogController', SpendDialogController);

    SpendDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Spend'];

    function SpendDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Spend) {
        var vm = this;

        vm.spend = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.spend.id !== null) {
                Spend.update(vm.spend, onSaveSuccess, onSaveError);
            } else {
                Spend.save(vm.spend, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:spendUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.date = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
