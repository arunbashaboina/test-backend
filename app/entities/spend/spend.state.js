(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('spend', {
            parent: 'entity',
            url: '/spend?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.spend.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/spend/spends.html',
                    controller: 'SpendController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('spend');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('spend-detail', {
            parent: 'spend',
            url: '/spend/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.spend.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/spend/spend-detail.html',
                    controller: 'SpendDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('spend');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Spend', function($stateParams, Spend) {
                    return Spend.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'spend',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('spend-detail.edit', {
            parent: 'spend-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/spend/spend-dialog.html',
                    controller: 'SpendDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Spend', function(Spend) {
                            return Spend.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('spend.new', {
            parent: 'spend',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/spend/spend-dialog.html',
                    controller: 'SpendDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                user: null,
                                spendCategory: null,
                                supplier: null,
                                referenceContract: null,
                                date: null,
                                legalan: null,
                                purpose: null,
                                amount: null,
                                details: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('spend', null, { reload: 'spend' });
                }, function() {
                    $state.go('spend');
                });
            }]
        })
        .state('spend.edit', {
            parent: 'spend',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/spend/spend-dialog.html',
                    controller: 'SpendDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Spend', function(Spend) {
                            return Spend.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('spend', null, { reload: 'spend' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('spend.delete', {
            parent: 'spend',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/spend/spend-delete-dialog.html',
                    controller: 'SpendDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Spend', function(Spend) {
                            return Spend.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('spend', null, { reload: 'spend' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
