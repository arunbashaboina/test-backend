(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SpendDetailController', SpendDetailController);

    SpendDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Spend'];

    function SpendDetailController($scope, $rootScope, $stateParams, previousState, entity, Spend) {
        var vm = this;

        vm.spend = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:spendUpdate', function(event, result) {
            vm.spend = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
