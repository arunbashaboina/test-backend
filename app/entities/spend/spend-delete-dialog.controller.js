(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SpendDeleteController',SpendDeleteController);

    SpendDeleteController.$inject = ['$uibModalInstance', 'entity', 'Spend'];

    function SpendDeleteController($uibModalInstance, entity, Spend) {
        var vm = this;

        vm.spend = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Spend.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
