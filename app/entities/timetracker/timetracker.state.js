(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('timetracker', {
            parent: 'entity',
            url: '/timetracker?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.timetracker.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/timetracker/timetrackers.html',
                    controller: 'TimetrackerController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('timetracker');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('timetracker-detail', {
            parent: 'timetracker',
            url: '/timetracker/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.timetracker.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/timetracker/timetracker-detail.html',
                    controller: 'TimetrackerDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('timetracker');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Timetracker', function($stateParams, Timetracker) {
                    return Timetracker.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'timetracker',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('timetracker-detail.edit', {
            parent: 'timetracker-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/timetracker/timetracker-dialog.html',
                    controller: 'TimetrackerDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Timetracker', function(Timetracker) {
                            return Timetracker.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('timetracker.new', {
            parent: 'timetracker',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/timetracker/timetracker-dialog.html',
                    controller: 'TimetrackerDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                user: null,
                                date: null,
                                duration: null,
                                purpose: null,
                                referenceContract: null,
                                details: null,
                                referenceLegalConcern: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('timetracker', null, { reload: 'timetracker' });
                }, function() {
                    $state.go('timetracker');
                });
            }]
        })
        .state('timetracker.edit', {
            parent: 'timetracker',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/timetracker/timetracker-dialog.html',
                    controller: 'TimetrackerDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Timetracker', function(Timetracker) {
                            return Timetracker.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('timetracker', null, { reload: 'timetracker' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('timetracker.delete', {
            parent: 'timetracker',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/timetracker/timetracker-delete-dialog.html',
                    controller: 'TimetrackerDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Timetracker', function(Timetracker) {
                            return Timetracker.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('timetracker', null, { reload: 'timetracker' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
