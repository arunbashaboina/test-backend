(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('TimetrackerDetailController', TimetrackerDetailController);

    TimetrackerDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Timetracker'];

    function TimetrackerDetailController($scope, $rootScope, $stateParams, previousState, entity, Timetracker) {
        var vm = this;

        vm.timetracker = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:timetrackerUpdate', function(event, result) {
            vm.timetracker = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
