(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('TimetrackerDialogController', TimetrackerDialogController);

    TimetrackerDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Timetracker'];

    function TimetrackerDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Timetracker) {
        var vm = this;

        vm.timetracker = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.timetracker.id !== null) {
                Timetracker.update(vm.timetracker, onSaveSuccess, onSaveError);
            } else {
                Timetracker.save(vm.timetracker, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:timetrackerUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.date = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
