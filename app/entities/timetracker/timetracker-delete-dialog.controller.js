(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('TimetrackerDeleteController',TimetrackerDeleteController);

    TimetrackerDeleteController.$inject = ['$uibModalInstance', 'entity', 'Timetracker'];

    function TimetrackerDeleteController($uibModalInstance, entity, Timetracker) {
        var vm = this;

        vm.timetracker = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Timetracker.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
