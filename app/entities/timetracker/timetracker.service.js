(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('Timetracker', Timetracker);

    Timetracker.$inject = ['$resource', 'DateUtils'];

    function Timetracker ($resource, DateUtils) {
        var resourceUrl =  'api/timetrackers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.date = DateUtils.convertDateTimeFromServer(data.date);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
