(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('WorkflowDetailController', WorkflowDetailController);

    WorkflowDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Workflow'];

    function WorkflowDetailController($scope, $rootScope, $stateParams, previousState, entity, Workflow) {
        var vm = this;

        vm.workflow = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:workflowUpdate', function(event, result) {
            vm.workflow = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
