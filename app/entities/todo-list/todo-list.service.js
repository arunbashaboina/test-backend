(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('TodoList', TodoList);

    TodoList.$inject = ['$resource'];

    function TodoList ($resource) {
        var resourceUrl =  'api/todo-lists/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
