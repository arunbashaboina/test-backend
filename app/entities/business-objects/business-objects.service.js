(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('BusinessObjects', BusinessObjects);

    BusinessObjects.$inject = ['$resource'];

    function BusinessObjects ($resource) {
        var resourceUrl =  'api/business-objects/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
