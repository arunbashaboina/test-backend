(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('BusinessObjectsDetailController', BusinessObjectsDetailController);

    BusinessObjectsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'BusinessObjects'];

    function BusinessObjectsDetailController($scope, $rootScope, $stateParams, previousState, entity, BusinessObjects) {
        var vm = this;

        vm.businessObjects = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:businessObjectsUpdate', function(event, result) {
            vm.businessObjects = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
