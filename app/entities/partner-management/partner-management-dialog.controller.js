(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('Partner_managementDialogController', Partner_managementDialogController);

    Partner_managementDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Partner_management'];

    function Partner_managementDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Partner_management) {
        var vm = this;

        vm.partner_management = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.partner_management.id !== null) {
                Partner_management.update(vm.partner_management, onSaveSuccess, onSaveError);
            } else {
                Partner_management.save(vm.partner_management, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:partner_managementUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
