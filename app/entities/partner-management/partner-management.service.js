(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('Partner_management', Partner_management);

    Partner_management.$inject = ['$resource'];

    function Partner_management ($resource) {
        var resourceUrl =  'api/partner-managements/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

