(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('Partner_managementDetailController', Partner_managementDetailController);

    Partner_managementDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Partner_management'];

    function Partner_managementDetailController($scope, $rootScope, $stateParams, previousState, entity, Partner_management) {
        var vm = this;

        vm.partner_management = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:partner_managementUpdate', function(event, result) {
            vm.partner_management = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
