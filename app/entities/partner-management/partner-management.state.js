(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('partner-management', {
            parent: 'entity',
            url: '/partner-management?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.partner_management.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/partner-management/partner-managements.html',
                    controller: 'Partner_managementController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('partner_management');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('partner-management-detail', {
            parent: 'partner-management',
            url: '/partner-management/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.partner_management.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/partner-management/partner-management-detail.html',
                    controller: 'Partner_managementDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('partner_management');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Partner_management', function($stateParams, Partner_management) {
                    return Partner_management.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'partner-management',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('partner-management-detail.edit', {
            parent: 'partner-management-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/partner-management/partner-management-dialog.html',
                    controller: 'Partner_managementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Partner_management', function(Partner_management) {
                            return Partner_management.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('partner-management.new', {
            parent: 'partner-management',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/partner-management/partner-management-dialog.html',
                    controller: 'Partner_managementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                partnerName: null,
                                partnerType: null,
                                address: null,
                                partnerId: null,
                                dataObjects: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('partner-management', null, { reload: 'partner-management' });
                }, function() {
                    $state.go('partner-management');
                });
            }]
        })
        .state('partner-management.edit', {
            parent: 'partner-management',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/partner-management/partner-management-dialog.html',
                    controller: 'Partner_managementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Partner_management', function(Partner_management) {
                            return Partner_management.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('partner-management', null, { reload: 'partner-management' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('partner-management.delete', {
            parent: 'partner-management',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/partner-management/partner-management-delete-dialog.html',
                    controller: 'Partner_managementDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Partner_management', function(Partner_management) {
                            return Partner_management.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('partner-management', null, { reload: 'partner-management' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
