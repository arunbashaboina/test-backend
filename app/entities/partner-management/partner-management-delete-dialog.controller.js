(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('Partner_managementDeleteController',Partner_managementDeleteController);

    Partner_managementDeleteController.$inject = ['$uibModalInstance', 'entity', 'Partner_management'];

    function Partner_managementDeleteController($uibModalInstance, entity, Partner_management) {
        var vm = this;

        vm.partner_management = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Partner_management.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

