(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('Partner_registration', Partner_registration);

    Partner_registration.$inject = ['$resource'];

    function Partner_registration ($resource) {
        var resourceUrl =  'api/partner-registrations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();


