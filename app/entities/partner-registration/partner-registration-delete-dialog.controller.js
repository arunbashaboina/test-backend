(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('Partner_registrationDeleteController',Partner_registrationDeleteController);

    Partner_registrationDeleteController.$inject = ['$uibModalInstance', 'entity', 'Partner_registration'];

    function Partner_registrationDeleteController($uibModalInstance, entity, Partner_registration) {
        var vm = this;

        vm.partner_registration = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Partner_registration.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

