(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('partner-registration', {
            parent: 'entity',
            url: '/partner-registration?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.partner_registration.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/partner-registration/partner-registrations.html',
                    controller: 'Partner_registrationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('partner_registration');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('partner-registration-detail', {
            parent: 'partner-registration',
            url: '/partner-registration/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.partner_registration.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/partner-registration/partner-registration-detail.html',
                    controller: 'Partner_registrationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('partner_registration');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Partner_registration', function($stateParams, Partner_registration) {
                    return Partner_registration.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'partner-registration',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('partner-registration-detail.edit', {
            parent: 'partner-registration-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/partner-registration/partner-registration-dialog.html',
                    controller: 'Partner_registrationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Partner_registration', function(Partner_registration) {
                            return Partner_registration.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('partner-registration.new', {
            parent: 'partner-registration',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/partner-registration/partner-registration-dialog.html',
                    controller: 'Partner_registrationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                companyName: null,
                                firstName: null,
                                lastName: null,
                                emailAddress: null,
                                phoneNumber: null,
                                contactPersonName: null,
                                contactPersonEmail: null,
                                comments: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('partner-registration', null, { reload: 'partner-registration' });
                }, function() {
                    $state.go('partner-registration');
                });
            }]
        })
        .state('partner-registration.edit', {
            parent: 'partner-registration',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/partner-registration/partner-registration-dialog.html',
                    controller: 'Partner_registrationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Partner_registration', function(Partner_registration) {
                            return Partner_registration.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('partner-registration', null, { reload: 'partner-registration' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('partner-registration.delete', {
            parent: 'partner-registration',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/partner-registration/partner-registration-delete-dialog.html',
                    controller: 'Partner_registrationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Partner_registration', function(Partner_registration) {
                            return Partner_registration.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('partner-registration', null, { reload: 'partner-registration' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
