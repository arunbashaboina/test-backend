(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('Partner_registrationDialogController', Partner_registrationDialogController);

    Partner_registrationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Partner_registration'];

    function Partner_registrationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Partner_registration) {
        var vm = this;

        vm.partner_registration = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.partner_registration.id !== null) {
                Partner_registration.update(vm.partner_registration, onSaveSuccess, onSaveError);
            } else {
                Partner_registration.save(vm.partner_registration, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:partner_registrationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
