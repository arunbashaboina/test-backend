(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('Partner_registrationDetailController', Partner_registrationDetailController);

    Partner_registrationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Partner_registration'];

    function Partner_registrationDetailController($scope, $rootScope, $stateParams, previousState, entity, Partner_registration) {
        var vm = this;

        vm.partner_registration = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:partner_registrationUpdate', function(event, result) {
            vm.partner_registration = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();