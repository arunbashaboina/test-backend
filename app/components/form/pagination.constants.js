(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
